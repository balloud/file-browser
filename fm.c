/* Dana Ballou
    CS 352
    Program 3
*/
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <libgen.h>
#include <ctype.h>
#define MAX_DEPTH	1
#define MAX_LINE	1024

struct tStruct{
	GtkTreeStore * tStore;
	GtkListStore * lStore;
};

void init(int argc, char * argv[]);
void buildTreeStore(GtkTreeStore * store, char * directory, GtkTreeIter * parent, int root);
void buildTreeView(GtkWidget * tree);
void item_selected (GtkWidget *selection, gpointer data);
void item_selected2 (GtkWidget *selection, gpointer lStore);
void item_selected3 (GtkWidget * selection, gpointer buffer);
void item_selected4 (GtkWidget * selection, gpointer addressBuffer);
void item_selected5 (GtkWidget * selection, gpointer acsiiBuffer);
int isDir(char * path);
void buildListStore(GtkListStore * store, char * directory);
off_t getSize(char *file);
char * getModTime(char * path);
char * getPerms(char * path);
void buildListView(GtkWidget * list);
void printHex(char * path);
int fillBuffer (char * path, GtkTextBuffer* buffer);
int fillAddressBuffer (char * path, GtkTextBuffer* buffer);
int fillAsciiBuffer (char * path, GtkTextBuffer* buffer);


int main (int argc, char *argv[]) {

	init(argc, argv);	
	return 0;

}

void init(int argc, char * argv[]) {
	GtkWidget *window, *hpaned, *hpaned2, *fsWin, *dirWin, *hexWin, *hexBox;

	  
	
	/* Initialize GTK+ and all of its supporting libraries. */
	gtk_init (&argc, &argv);
	/* Create a new window, give it a title and display it to the user. */
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	hpaned = gtk_paned_new (GTK_ORIENTATION_HORIZONTAL);
	hpaned2 = gtk_paned_new (GTK_ORIENTATION_VERTICAL);
	fsWin = gtk_scrolled_window_new(NULL, NULL);
	dirWin = gtk_scrolled_window_new(NULL, NULL);
	hexWin = gtk_scrolled_window_new(NULL, NULL);
	
	hexBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);	
    /* Closeing functionality added here */
	gtk_window_set_title (GTK_WINDOW (window), "File Manager");
	g_signal_connect (window, "delete_event", gtk_main_quit, NULL);	

    /* Initialize trees and stores here */
	GtkTreeStore * tStore = gtk_tree_store_new (3, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_STRING);
	GtkTreeIter * tsi; /* TreeStoreIterator */
	buildTreeStore(tStore, "/", tsi, 1);
	GtkWidget * tView = gtk_tree_view_new();
	buildTreeView(tView);
	gtk_tree_view_set_level_indentation (GTK_TREE_VIEW(tView), 20);
	gtk_tree_view_set_model (GTK_TREE_VIEW (tView), GTK_TREE_MODEL (tStore));

	
	GtkListStore * lStore = gtk_list_store_new (6, GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	
	buildListStore(lStore, "/");
	GtkWidget * lView = gtk_tree_view_new(); /* Treeview for list in right pane */
	buildListView(lView);
	gtk_tree_view_set_model (GTK_TREE_VIEW (lView), GTK_TREE_MODEL (lStore));


	GtkWidget* textview = gtk_text_view_new();

	GtkWidget* addressTextview = gtk_text_view_new();

	GtkWidget* asciiTextview = gtk_text_view_new();  

    /* Set sizes and pack widgets */
	gtk_widget_set_size_request (window, 1000, 800);
	gtk_widget_set_size_request (hpaned, 400, 800);
	gtk_widget_set_size_request (hpaned2, 400, 800);
	gtk_widget_set_size_request (fsWin, 250, 800);
	gtk_widget_set_size_request (dirWin, 550, 500);
	gtk_paned_pack1 (GTK_PANED (hpaned), fsWin, TRUE, TRUE);
	gtk_paned_pack2 (GTK_PANED (hpaned), hpaned2, TRUE, TRUE);
	gtk_paned_pack1 (GTK_PANED (hpaned2), dirWin, TRUE, TRUE);
	gtk_paned_pack2 (GTK_PANED (hpaned2), hexWin, TRUE, TRUE);
	gtk_box_pack_start(GTK_BOX (hexBox), addressTextview, FALSE, FALSE, 10);
	gtk_box_pack_start(GTK_BOX (hexBox), textview, FALSE, FALSE, 10);
	gtk_box_pack_start(GTK_BOX (hexBox), asciiTextview, FALSE, FALSE, 10);	
	gtk_container_add (GTK_CONTAINER (fsWin), tView);
	gtk_container_add (GTK_CONTAINER (dirWin), lView);	
	gtk_container_add (GTK_CONTAINER (window), hpaned);
	gtk_container_add (GTK_CONTAINER (window), fsWin);
	gtk_container_add (GTK_CONTAINER (window), dirWin);
	gtk_container_add (GTK_CONTAINER (window), hpaned2);
	gtk_container_add (GTK_CONTAINER (hexWin), hexBox);
	gtk_container_add (GTK_CONTAINER (window), hexWin);
	/*****************************************************************/
    /*************** added for selection handling ********************/
    // set the tree selection object
    GtkTreeSelection *selection;
    GtkTreeSelection *selection3; /* Selection from direcory window */
    selection3 = gtk_tree_view_get_selection (GTK_TREE_VIEW(lView));
    selection = gtk_tree_view_get_selection (GTK_TREE_VIEW(tView));
    struct tStruct * t = malloc(sizeof(*t));
    // connect the selection callback function
    g_signal_connect (G_OBJECT(selection), "changed", 
                      G_CALLBACK(item_selected), (gpointer) tStore);
    /******************************************************************/ 
    g_signal_connect (G_OBJECT(selection), "changed", 
                      G_CALLBACK(item_selected2), lStore);
    /******************************************************************/
    // connect the selection callback function
    g_signal_connect (G_OBJECT(selection3), "changed", 
                      G_CALLBACK(item_selected3), gtk_text_view_get_buffer(GTK_TEXT_VIEW (textview)));
    /******************************************************************/  
    g_signal_connect (G_OBJECT(selection3), "changed", 
                      G_CALLBACK(item_selected4), gtk_text_view_get_buffer(GTK_TEXT_VIEW (addressTextview)));
    /******************************************************************/
    // connect the selection callback function
    g_signal_connect (G_OBJECT(selection3), "changed", 
                      G_CALLBACK(item_selected5), gtk_text_view_get_buffer(GTK_TEXT_VIEW (asciiTextview)));
 	/******************************************************************/
	gtk_widget_show_all (window);
	gtk_main ();
}
void item_selected (GtkWidget * selection, gpointer tStore) {
    
    GtkTreeModel *model;
    GtkTreeIter iter;
    
    if (gtk_tree_selection_get_selected (GTK_TREE_SELECTION(selection), &model, &iter)) {        
      
        gchar * path;
        gtk_tree_model_get (model, &iter, 2, &path, -1);        
        // g_message("expanding directory: %s\n", path);
        buildTreeStore(tStore, path, &iter, 0);
    }
}
void item_selected2 (GtkWidget * selection, gpointer lStore) { /* For directory View */
    
    GtkTreeModel *model;
    GtkTreeIter iter;
    
    if (gtk_tree_selection_get_selected (GTK_TREE_SELECTION(selection), &model, &iter)) {        

        gchar * path;
        gtk_tree_model_get (model, &iter, 2, &path, -1);        
        // g_message("loading folder contents into directory pane: %s\n", path);
        buildListStore(lStore, path);
    }
}

void item_selected3 (GtkWidget * selection, gpointer buffer) {
    
    GtkTreeModel *model;
    GtkTreeIter iter;    
    if (gtk_tree_selection_get_selected (GTK_TREE_SELECTION(selection), &model, &iter)) {        
        gchar * path;
        gtk_tree_model_get (model, &iter, 5, &path, -1);        
        // g_message("DIRECTORY SELECTION: %s\n", path);
        fillBuffer(path, buffer);
    }
}

void item_selected4 (GtkWidget * selection, gpointer addressBuffer) { /*For addresses */
    
    GtkTreeModel *model;
    GtkTreeIter iter;
    
    if (gtk_tree_selection_get_selected (GTK_TREE_SELECTION(selection), &model, &iter)) {        
        gchar * path;
        gtk_tree_model_get (model, &iter, 5, &path, -1);        
        // g_message("DIRECTORY SELECTION: %s\n", path);
        fillAddressBuffer(path, addressBuffer);
    }
}

void item_selected5 (GtkWidget * selection, gpointer acsiiBuffer) { /*For ASCII view */
    
    GtkTreeModel *model;
    GtkTreeIter iter;    
    if (gtk_tree_selection_get_selected (GTK_TREE_SELECTION(selection), &model, &iter)) {
        
        gchar * path;
        gtk_tree_model_get (model, &iter, 5, &path, -1);        
        // g_message("DIRECTORY SELECTION: %s\n", path);
        fillAsciiBuffer(path, acsiiBuffer);
    }
}

int fillAddressBuffer (char * path, GtkTextBuffer* buffer) {
	/* First, clear old buffer */
  	GtkTextIter s; 
  	GtkTextIter e;
  	gtk_text_buffer_get_start_iter (buffer, &s); 
  	gtk_text_buffer_get_end_iter (buffer, &e); 
  	gtk_text_buffer_delete(buffer, &s, &e);
    GtkTextIter iter;  
    char line[MAX_LINE];
    char aBuffer[10];

    FILE* input;
    
    if ((input = fopen(path, "r")) == NULL) {
        printf("File %s not found\n", path);
        return 1;
    }
    // initialize the buffer's iterator
    gtk_text_buffer_get_iter_at_offset(buffer, &iter, 0);
    // add address to the buffer
    int address = 0;
    while (fgets(line, 17 * sizeof(char), input)) { 
    	sprintf(aBuffer, "%08X\n", address);
	    address += 16;	    	 
    	gtk_text_buffer_insert (buffer, &iter, aBuffer, -1);
    }    
    return 0;
}
int fillAsciiBuffer (char * path, GtkTextBuffer* buffer) {
	/* First, clear old buffer */
  	GtkTextIter s; 
  	GtkTextIter e;
  	gtk_text_buffer_get_start_iter (buffer, &s); 
  	gtk_text_buffer_get_end_iter (buffer, &e); 
  	gtk_text_buffer_delete(buffer, &s, &e);
    GtkTextIter iter;  
    char line[MAX_LINE];
    char lineASCII[MAX_LINE];
    FILE* input;
    
    if ((input = fopen(path, "r")) == NULL) {
        printf("File %s not found\n", path);
        return 1;
    }
    // initialize the buffer's iterator
    gtk_text_buffer_get_iter_at_offset(buffer, &iter, 0);
    // add each line from the file to the buffer   
    while (fgets(line, 17 * sizeof(char), input)) { 

    	int read = 0;
    	int len = 0;
    	while (read <= 15) {
            if (isprint(line[read])) {
	    	    len += sprintf(lineASCII+len, "%c", line[read]);
            } else {
                len += sprintf(lineASCII+len, "%c", '.');
            }
    		read++;
    	}
	    sprintf(lineASCII+len, "\n");	 
    	gtk_text_buffer_insert (buffer, &iter, lineASCII, -1);
    }    
    return 0;
}
int fillBuffer (char * path, GtkTextBuffer* buffer) {
	/* First, clear old buffer */
  	GtkTextIter s; 
  	GtkTextIter e;
  	gtk_text_buffer_get_start_iter (buffer, &s); 
  	gtk_text_buffer_get_end_iter (buffer, &e); 
  	gtk_text_buffer_delete(buffer, &s, &e);
    GtkTextIter iter;  
    char line[MAX_LINE];
    char lineHEX[MAX_LINE];
    FILE* input;    
    if ((input = fopen(path, "r")) == NULL) {
        printf("File %s not found\n", path);
        return 1;
    }

    // initialize the buffer's iterator
    gtk_text_buffer_get_iter_at_offset(buffer, &iter, 0);

    // add each line from the file to the buffer  
    while (fgets(line, 17 * sizeof(char), input)) { 
    	int read = 0;
    	int len = 0;
    	while (read <= 15) {
	    	len += sprintf(lineHEX+len, "%02X ", line[read]);
    		read++;
    	}
	    sprintf(lineHEX+len, "\n");	 
    	gtk_text_buffer_insert (buffer, &iter, lineHEX, -1);
    }    
    return 0;
}
void buildTreeStore(GtkTreeStore * store, char * directory, GtkTreeIter * parent, int root) {	
		
	GtkTreeIter tsi;
	GError *error = NULL;
	GdkPixbuf* folder = gdk_pixbuf_new_from_file("folder.png", &error);     
    struct dirent *directoryEntry; 
	DIR *directoryPointer;
	directoryPointer = opendir (directory);
	if (directoryPointer != NULL) {

		while (directoryEntry = readdir (directoryPointer)) {
	
			char path[MAX_LINE];			
			if (root) {
				sprintf(path, "%s%s", directory, directoryEntry->d_name);
			} else {
				sprintf(path, "%s/%s", directory, directoryEntry->d_name);
			}
			/* Check for subfolders to recurse on */
			if (isDir(path)) {
				if (root) {
					gtk_tree_store_append (store, &tsi, NULL);
				} else {
					gtk_tree_store_append (store, &tsi, parent);
				}		
				gtk_tree_store_set(store, &tsi, 0, folder, 1, directoryEntry->d_name, 2, path, -1);				
			}			
		}
		(void) closedir (directoryPointer);
	}
	else {
		perror ("Couldn't open the directory");
	}
}

void buildTreeView(GtkWidget * tree) {

	GtkCellRenderer * renderer;
    GtkTreeViewColumn * column;

    renderer = gtk_cell_renderer_pixbuf_new();
    column = gtk_tree_view_column_new_with_attributes("", renderer, "pixbuf", 0, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", 1, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);
}

int isDir(char * path) {

	/* Check if a path is a directory or not! */
	struct stat st;
    stat(path, &st);
    return S_ISDIR(st.st_mode);
}
off_t getSize(char * path) {
    struct stat st;
    stat(path, &st);
    return st.st_size;
}
char * getModTime(char * path) {
	struct stat st;
    stat(path, &st);
    return (ctime(&st.st_mtime));
}
char * getPerms(char * path) {
	struct stat st;
    stat(path, &st);
    char * permString;
    memset(permString, 0, MAX_LINE * sizeof(char));
	int indicator;       
 
        /* print a leading dash as start of file/directory permissions */        
        indicator = st.st_mode;
        if (S_ISDIR(indicator)) {
        	strcat(permString,"d");
        } else {
			strcat(permString,"-");
        }
        /* Check owner permissions */
        if ((indicator & S_IRUSR) && (indicator & S_IREAD))
          strcat(permString,"r");
        else
          strcat(permString,"-");
        if ((indicator & S_IWUSR) && (indicator & S_IWRITE)) 
          strcat(permString,"w");
        else
          strcat(permString,"-");
        if ((indicator & S_IXUSR) && (indicator & S_IEXEC))
          strcat(permString,"x");
        else
          strcat(permString,"-");
        /* Check group permissions */
        if ((indicator & S_IRGRP) && (indicator & S_IREAD))
          strcat(permString,"r");
        else
          strcat(permString,"-");
        if ((indicator & S_IWGRP) && (indicator & S_IWRITE))
          strcat(permString,"w");
        else
          strcat(permString,"-");
        if ((indicator & S_IXGRP) && (indicator & S_IEXEC))
          strcat(permString,"x");
        else
          strcat(permString,"-");
        /* check other user permissions */
        if ((indicator & S_IROTH) && (indicator & S_IREAD))
          strcat(permString,"r");
        else
          strcat(permString,"-");
        if ((indicator & S_IWOTH) && (indicator & S_IWRITE))
          strcat(permString,"w");
        else
          strcat(permString,"-");
        if ((indicator & S_IXOTH) && (indicator & S_IEXEC))
         
          strcat(permString,"x");

      	/* Return string of permissinos back to caller */

      	return permString;
}

void buildListStore(GtkListStore * store, char * directory) {

	gtk_list_store_clear(store); /* Clear old store */
	GError *error = NULL;
	GdkPixbuf* icon;	
	GtkTreeIter lsi;
	DIR *directoryPointer;
	struct dirent *directoryEntry;     
	directoryPointer = opendir (directory);
	if (directoryPointer != NULL) {
		while (directoryEntry = readdir (directoryPointer)) {			
			char path[MAX_LINE];
			char newPath[MAX_LINE];			
			sprintf(path, "%s/%s", directory, directoryEntry->d_name);
			char * base = directoryEntry->d_name;	
			off_t size = getSize(path);
			char * modTime = getModTime(path);
			char * perms = getPerms(path);
			gtk_list_store_append (store, &lsi);
			sprintf(newPath, "%s/%s", directory, directoryEntry->d_name);		
			if (isDir(newPath)) {
				icon = gdk_pixbuf_new_from_file("cicon.png", &error);
			} else {
				icon = gdk_pixbuf_new_from_file("texticon.png", &error);
			}
			gtk_list_store_set (store, &lsi, 0, icon, 1, base, 2, size, 3, modTime, 4, perms, 5, newPath, -1);	
		}
		(void) closedir (directoryPointer);
	}
	else {
		perror ("Couldn't open the directory");
	}
}
void buildListView(GtkWidget * list) {
	GtkCellRenderer * renderer;
    GtkTreeViewColumn * column;

    renderer = gtk_cell_renderer_pixbuf_new();
    column = gtk_tree_view_column_new_with_attributes("", renderer, "pixbuf", 0, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", 1, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
    gtk_tree_view_column_set_sort_column_id(column, 1);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes("Size", renderer, "text", 2, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
    gtk_tree_view_column_set_sort_column_id(column, 2);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes("Date Modified", renderer, "text", 3, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
    gtk_tree_view_column_set_sort_column_id(column, 3);
    gtk_tree_view_column_set_sort_indicator(column, TRUE);

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes("Permissions", renderer, "text", 4, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
}

