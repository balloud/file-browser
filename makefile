CC = gcc
CFLAGS = -Wall -g `pkg-config gtk+-3.0 --cflags --libs`
LDFLAGS = -g -o
LIBS = `pkg-config gtk+-3.0 --cflags --libs`
OBJECTS = fm.o

fm:	$(OBJECTS)
	clear
	gcc -g -o fm fm.c `pkg-config gtk+-3.0 --cflags --libs`

%.o:	%.c
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	clear
	rm -f *.o $(EXES)